## NuConta Marketplace App

Aplicación para poder comprar productos.

### > Solución

Se utilizaron los siguientes plugins de flutter para su implementación:

* **graphql_flutter: ^5.0.0**. Para la implementación de GraphQL en la obtención de datos y la petición para hacer la compra de las ofertas.
* **photo_view: ^0.12.0**. Para la vista en detalle de la foto del producto.
* **shared_preferences: ^2.0.6** Para guardar el saldo que tiene el usuario y actualizarlo cada vez que hace una compra.

El código del proyecto se dividió en la siguiente estructura, también se explica lo que hace de manera general cada clase implementada:

```
lib
    src
        pages
            home.page.dart              // Página para el inicio de la aplicación, donde se muestra el balance y las ofertas
            product-detail.page.dart    // Se muestra el detalle del producto y la opción para comprarlo   
        providers
            querys
                queries-g.dart               // Querys para la consulta de datos y la compra del producto
            graphql.provider.dart       // Provider para el manejo de peticiones en GraphQL
        utils
            user-preferences.dart       // Clase para el manejo de las preferencias de usuario (solo se guarda el balance)
        widgets
            dialog.widget.dart          // Clase para gestionar los diálogos mostrados en la app
            hero-photo-view-route-wrapper.widget.dart    // Widget para gestionar la animación y vista de las imágenes con un viewer
        env.dart            // Clase con variables generales de la app (incluye endpoint, token y nombre de la app)
    main.dart               // Clase principal de Flutter

``` 

Se utilizaron carpetas para separar los archivos según su objetivo, estas carpetas se definieron así:

* `pages` Sirve para guardar todas las pantallas implementadas en la app.

* `providers` Sirve para guardar todos los *providers* de la app, comúnmente aquí se guardan las clases con manejo de storage, peticiones a APIs, manejo de cámara o diferentes sensores, etc. 

* `utils` Sirve para guardar las clases que llevan algún proceso y son usados en muchas pantallas.

* `widgets` Sirve para guardar las clases que tienen un Widget con funcionalidad y que puede ser usado en diferentes pantallas. 

### > Requerimientos
* Instalar Flutter 2.2.1

Para Android:

* Descargar Android Studio y el SDK.

* Tener un dispositivo android o correr un simulador desde Android Studio.

Para iOS (necesario MacOS)

* Instalar XCode.

* Instalar cocopods.

### > Ejecutar la app.

1. Clonar repositorio en alguna parte de tu máquina.
2. Abrir una terminal en la dirección del repositorio y ejecutar
`flutter pub get`
para descargar las dependencias del proyecto (plugins y otras cosas).
3. Para ejecutar la aplicación en Android, es necesario tener conectado un dispositivo o tener un simulador corriendo y ejecutar `flutter run lib/main.dart`.
Para ejecutar en iOS, es necesario tener conectado un dispositivo o tener un simulador de iOS generado en XCode activo y ejecutar `flutter run lib/main.dart`.

**NOTA:** En caso de que se requiera utilizar desde cero la app. Es necesario salir de la app y volver a entrar para que el saldo se actualice.

### > Desarrollado por:
* *Ernesto Jacobo Troncoso de la Riva*
 