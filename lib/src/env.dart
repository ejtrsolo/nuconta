class Env {
    static const String APP_NAME = "NuConta Marketplace";
    static const String ENDPOINT = "https://staging-nu-needful-things.nubank.com.br/query";
    static const String ACCESS_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhd2Vzb21lY3VzdG9tZXJAZ21haWwuY29tIn0.cGT2KqtmT8KNIJhyww3T8fAzUsCD5_vxuHl5WbXtp8c';
}