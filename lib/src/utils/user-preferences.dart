import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {

    static final UserPreferences _instancia = new UserPreferences._internal();

    factory UserPreferences() {
        return _instancia;
    }

    UserPreferences._internal();

    SharedPreferences? _prefs;

    initPrefs() async {
        this._prefs = await SharedPreferences.getInstance();
    }

    // GET y SET del balance
    double get balance {
        return _prefs?.getDouble('balance') ?? 0 ;
    }
    set balance( double value ) {
        _prefs?.setDouble('balance', value);
    } 
    //GET y SET del init
    bool get init {
        return _prefs?.getBool('init') ?? false ;
    }
    set init( bool value ) {
        _prefs?.setBool('init', value);
    }


    // Metodo para cerrar sesión
    void cleanAll(){
        this.balance = 0.0;
        this.init = false;
    }

}