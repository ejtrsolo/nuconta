
class QueriesG {
    static const String read = r'''
{
    viewer{
        id
        name
        balance
        offers{
            id
            price
            product{
                id
                name
                description
                image
            }
        }
    }
}
''';

    static const String buy = r'''
mutation Purchase($offerId: ID!){
  purchase(offerId: $offerId){
    success
    errorMessage
    customer {
      id
      name
      balance
    }
  }
}
''';

}