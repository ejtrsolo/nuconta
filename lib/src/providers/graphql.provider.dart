import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:nuconta/src/env.dart';

class GraphqlProvider {
    final HttpLink httpLink = new HttpLink(Env.ENDPOINT);
    final AuthLink authLink = AuthLink(
        getToken: () async => 'Bearer ${Env.ACCESS_TOKEN}',
    );

    /// Obtener cliente de GraphQL para el widget de consulta
    ValueNotifier<GraphQLClient> getClient() {
        final Link link = authLink.concat(httpLink);
        ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
            GraphQLClient(
                link: link,
                cache: GraphQLCache(),
            ),
        );
        return client;
    }

    /// Función para ejecutar una mutación en GraphQL
    Future<QueryResult> executeMutation(String query, Map<String, dynamic> params){
        final Link link = authLink.concat(httpLink);
        final c = GraphQLClient(
            link: link,
            cache: GraphQLCache(),
        );
        return c.mutate(MutationOptions(document: gql(query), variables: params));
    }
}