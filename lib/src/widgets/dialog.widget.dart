import 'package:flutter/material.dart';

class DialogWidget {

    /// Función que genera un Widget de "loading" que muestra un texto en esa animación 
    static Future<void> showLoadingDialog(BuildContext context, {String message: 'Loading...'}) async {
        return showDialog<void>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
                return new WillPopScope(
                onWillPop: () async => false,
                child: SimpleDialog(
                    // key: key,
                    backgroundColor: Colors.black54,
                    children: <Widget>[
                    Center(
                        child: Column(children: [
                            CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white)),
                            SizedBox(height: 10),
                            Text(message, style: TextStyle(color: Colors.white))
                        ]),
                    )
                    ]
                )
                );
            }
        );
    }

    /// Quitar widget de loading
    static void dismissLoading(BuildContext context){
        Navigator.of(context, rootNavigator: true).pop();
    }

    /// Muestra un mensaje en pantalla y espera confirmación de leído.
    static Future showAlertDialog(BuildContext context, String title, String message) {
        return showDialog(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text(title),
                    content: Text(message),
                    actions: [
                        TextButton(
                            child: Text("OK"),
                            onPressed: () {
                                Navigator.of(context).pop(); 
                            },
                        )
                    ],
                );
            },
        );
    }

    /// Muestra un mensaje en pantalla y realiza un proceso cuando da en OK
    static void showConfirmDialog(BuildContext context, String title, String message, Function okFunction, {Function? cancelFunction}) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text(title),
                    content: Text(message),
                    actions: [
                        TextButton(
                            child: Text("OK"),
                            onPressed: (){
                                Navigator.of(context).pop(); 
                                okFunction.call();
                            },
                        ),
                        TextButton(
                            child: Text("CANCEL"),
                            onPressed: () {
                                Navigator.of(context).pop(); 
                                cancelFunction!.call();
                            },
                        )
                    ],
                );
            },
        );
    }
}