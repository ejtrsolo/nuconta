import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:nuconta/src/providers/graphql.provider.dart';
import 'package:nuconta/src/providers/querys/queries-g.dart';
import 'package:nuconta/src/utils/user-preferences.dart';
import 'package:nuconta/src/widgets/dialog.widget.dart';
import 'package:nuconta/src/widgets/hero-photo-view-route-wrapper.widget.dart';

class ProductDetailPage extends StatefulWidget {
    static String routeName = 'product-detail';
    ProductDetailPage({Key? key}) : super(key: key);

    @override
    _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
    Map product = new Map();
    String url = '';
    UserPreferences prefs = new UserPreferences();

    @override
    Widget build(BuildContext context) {
        this.product = ModalRoute.of(context)!.settings.arguments as Map;
        this.url = this.product['product']['image'];
        final balanceFormatted = NumberFormat.currency(name: '').format(this.prefs.balance);
        final priceFormatted = NumberFormat.currency(name: '').format(this.product['price']*1.0);
        return Scaffold(
            appBar: AppBar(
                title: Text(this.product['product']['name']),
            ),
            body: ListView(
                children: [
                    Container(
                        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                Icon(Icons.credit_card),
                                Text('Balance: $balanceFormatted', style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center),
                            ],
                        ),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            Text('Click the imagen to zoom'),
                        ],
                    ),
                    Center(
                        child: GestureDetector(
                            onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HeroPhotoViewRouteWrapper(
                                            imageProvider: NetworkImage(this.url),
                                            maxScale: PhotoViewComputedScale.covered * 2.0,
                                            minScale: PhotoViewComputedScale.contained * 0.8,
                                        ),
                                    ),
                                );
                            },
                            child: Container(
                                color: Colors.grey[200],
                                padding: EdgeInsets.all(16),
                                child: Hero(
                                    tag: "Product image",
                                    child: Image.network(
                                        this.url,
                                        width: 300.0,
                                        loadingBuilder: (_, child, chunk) => chunk != null ? const Text("loading") : child,
                                    ),
                                ),
                            ),
                        ),
                    ),
                    SizedBox(height: 16),
                    Container(
                        child: Text('\$$priceFormatted - ${this.product['product']['name']}', textScaleFactor: 2, style: TextStyle(fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.symmetric(horizontal: 16.0),
                    ),
                    SizedBox(height: 20),
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                            children: [
                                Text('Description', textScaleFactor: 1.6),
                            ],
                        ),
                    ),
                    SizedBox(height: 10.0,),
                    Container(
                        child: Text(this.product['product']['description'], textAlign: TextAlign.justify),
                        margin: EdgeInsets.symmetric(horizontal: 16.0),
                    ),
                    SizedBox(height: 20),
                    OutlinedButton(
                        child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                            child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                    Image(image: AssetImage("assets/icons/shopping.png"), height: 35.0),
                                    Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Text('Buy', style: TextStyle(fontSize: 20, color: Color.fromRGBO(18, 119, 243, 1))),
                                    )
                                ],
                            ),
                        ),
                        onPressed: () {
                            DialogWidget.showConfirmDialog(context, 'Alert', '¿Do you want to buy this product?', (){
                                if(this.prefs.balance < this.product['price']){
                                    String message = 'Your balance is insufficient. You can\'t buy this product.';
                                    DialogWidget.showAlertDialog(context, 'Error', message);
                                    return false;
                                }
                                final graphQLProvider = GraphqlProvider();
                                DialogWidget.showLoadingDialog(context);
                                graphQLProvider.executeMutation(QueriesG.buy, {'offerId' : this.product['id']}).then((result) {
                                    DialogWidget.dismissLoading(context);
                                    if(result.data!['purchase']['success']){
                                        this.prefs.balance = this.prefs.balance - this.product['price']; 
                                        DialogWidget.showAlertDialog(context, 'Successful', 'Successful purchase').then((value) {
                                            Navigator.of(context).pop();
                                        });
                                    } else {
                                        DialogWidget.showAlertDialog(context, 'Error', result.data!['purchase']['errorMessage']);
                                    }
                                }).catchError((error){
                                    DialogWidget.showAlertDialog(context, 'Error', error.toString());
                                });
                            });
                        },
                    )
                ],
            ),
        );
    }
}