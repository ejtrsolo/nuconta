import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';
import 'package:nuconta/src/pages/product-detail.page.dart';
import 'package:nuconta/src/providers/graphql.provider.dart';
import 'package:nuconta/src/providers/querys/queries-g.dart';
import 'package:nuconta/src/utils/user-preferences.dart';

class HomePage extends StatefulWidget {
    static String routeName = 'home';

    @override
    _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
    String name = 'User';
    double balance = 0.0;

    final prefs = new UserPreferences();

    @override
    void initState() { 
      super.initState();
      this.prefs.init = false;
    }
    
    @override
    Widget build(BuildContext context) {
        final graphqlProvider = new GraphqlProvider();
        return GraphQLProvider(
            client: graphqlProvider.getClient(),
            child: Scaffold(
                appBar: AppBar(
                    title: Text('NuConta Marketplace'),
                ),
                body: this.loadOffers()
            ),
        );
    }

    Widget loadOffers(){
        return Query(
            options: QueryOptions(
                document: gql(QueriesG.read), 
                pollInterval: Duration(seconds: 10),
            ),
            builder: (QueryResult result, { VoidCallback? refetch, FetchMore? fetchMore}) {
                if (result.hasException) {
                    return Text(result.exception.toString());
                }

                if (result.isLoading) {
                    return Center(
                      child: Row(
                        children: [
                            Image.asset(
                                'assets/animations/loading2.gif', 
                                width: MediaQuery.of(context).size.width
                            ),
                        ],
                      )
                    );
                }

                if(!this.prefs.init){
                    this.prefs.init = true;
                    this.prefs.balance = result.data!['viewer']['balance']*1.0;
                }

                this.name = result.data!['viewer']['name'];
                List offers = result.data!['viewer']['offers'];

                final balanceFormatted = NumberFormat.currency(name: '').format(this.prefs.balance);

                return Column(
                    children: <Widget>[
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                    Icon(Icons.person),
                                    Text('Hi $name', style: TextStyle(fontSize: 18.0), textAlign: TextAlign.center),
                                ],
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 16, bottom: 6) ,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    Text('Your balance is:', style: TextStyle(fontSize: 16.0), textAlign: TextAlign.center),
                                ],
                            ),
                        ),
                        Container(
                            margin: EdgeInsets.only(bottom: 20) ,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    Text('\$ $balanceFormatted', style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                                ],
                            ),
                        ),
                        Row(
                            children: [
                                SizedBox(width: 16),
                                Text('Offers', style: TextStyle(fontSize: 20.0)),
                            ],
                        ),
                        Divider(),
                        Expanded(
                            child: ListView.builder(
                                itemCount: offers.length,
                                itemBuilder: (BuildContext context, int index) {
                                    final offer = offers[index];
                                    final priceFormatted = NumberFormat.currency(name: '').format(offer['price']*1.0);
                                    return InkWell(
                                        child: Card(
                                            elevation: 1.0,
                                            child: Container(
                                                child: Column(
                                                    children: <Widget>[
                                                    ListTile(
                                                        leading: CircleAvatar(
                                                            radius: 30.0,
                                                            backgroundImage: NetworkImage('${offer['product']['image']}'),
                                                            backgroundColor: Colors.transparent,
                                                        ),
                                                        title: Text('\$ $priceFormatted', textScaleFactor: 1.4),
                                                        subtitle: Text('${offer['product']['name']}', textScaleFactor: 1.2),
                                                    ),
                                                    ]
                                                ),
                                            )
                                        ),
                                        onTap: (){
                                            Navigator.of(context).pushNamed(ProductDetailPage.routeName, arguments: offer).then((result){
                                                setState(() {});
                                            });
                                        },
                                    );
                                }
                            ),
                        ),
                    ],
                );
            },
        );
    }
}