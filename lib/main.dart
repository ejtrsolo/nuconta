import 'package:flutter/material.dart';
import 'package:nuconta/src/env.dart';
import 'package:nuconta/src/pages/home.page.dart';
import 'package:nuconta/src/pages/product-detail.page.dart';
import 'package:nuconta/src/utils/user-preferences.dart';


Future<void> main() async {
    WidgetsFlutterBinding.ensureInitialized();
    UserPreferences prefs = new UserPreferences();
    await prefs.initPrefs();

    runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
    GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            navigatorKey: _navigatorKey,
            debugShowCheckedModeBanner: false,
            title: Env.APP_NAME,
            initialRoute: HomePage.routeName,
            routes: {
                HomePage.routeName  : ( BuildContext context ) => HomePage(),
                ProductDetailPage.routeName  : ( BuildContext context ) => ProductDetailPage(),
            },
            theme: ThemeData(
                primaryColor: Colors.black,
            ),
        );
    }
}